from unittest import TestCase

from min_heap import MinHeap


class TestMinHeapGet(TestCase):

    def testGetNotInQueue(self):
        mh = MinHeap()

        for i in range(10):
            self.assertIsNone(mh.get_node_at(i))

    def testGetInts(self):
        mh = MinHeap()

        for i in range(10):
            mh.append(i)

        for i in range(10):
            self.assertEqual(i, mh.get_node_at(i))

    def testGetStrings(self):
        mh = MinHeap()

        for i in range(10):
            mh.append(str(i))

        for i in range(10):
            self.assertEqual(str(i), mh.get_node_at(i))


class TestMinHeapIsEmpty(TestCase):

    def testIsEmptyAfterInstanciation(self):
        mh = MinHeap()
        self.assertTrue(mh.is_empty())

    def testIsEmptyAfterAppendThenPopHead(self):
        mh = MinHeap()

        mh.append(1)
        mh.pop_min()

        self.assertTrue(mh.is_empty())


class TestMinHeapLength(TestCase):

    def testLengths(self):
        mh = MinHeap()
        self.assertEqual(0, len(mh))

        mh.append(1)
        self.assertEqual(1, len(mh))

        mh.append(0)
        self.assertEqual(2, len(mh))

        mh.pop_min()
        self.assertEqual(1, len(mh))

        mh.pop_min()
        self.assertEqual(0, len(mh))


class TestMinHeapAppend(TestCase):

    def testAppendTwoUnsortedElements(self):
        mh = MinHeap()

        mh.append(1)
        mh.append(0)

        self.assertEqual(0, mh.get_node_at(0))
        self.assertEqual(1, mh.get_node_at(1))

    def testAppendThreeUnsortedElements(self):
        mh = MinHeap()

        mh.append(2)
        mh.append(1)
        self.assertEqual(1, mh.get_node_at(0))
        self.assertEqual(2, mh.get_node_at(1))

        mh.append(0)
        self.assertEqual(0, mh.get_node_at(0))
        self.assertEqual(2, mh.get_node_at(1))
        self.assertEqual(1, mh.get_node_at(2))

    def testAppendFourUnsortedElements(self):
        mh = MinHeap()

        mh.append(4)
        mh.append(2)
        self.assertEqual(2, mh.get_node_at(0))
        self.assertEqual(4, mh.get_node_at(1))

        mh.append(3)
        self.assertEqual(2, mh.get_node_at(0))
        self.assertEqual(4, mh.get_node_at(1))
        self.assertEqual(3, mh.get_node_at(2))

        mh.append(1)
        self.assertEqual(1, mh.get_node_at(0))
        self.assertEqual(2, mh.get_node_at(1))
        self.assertEqual(3, mh.get_node_at(2))
        self.assertEqual(4, mh.get_node_at(3))

    def testAppendFiveUnsortedElements(self):
        mh = MinHeap()

        mh.append(4)
        mh.append(2)
        mh.append(3)
        mh.append(1)

        mh.append(0)
        self.assertEqual(0, mh.get_node_at(0))
        self.assertEqual(1, mh.get_node_at(1))
        self.assertEqual(3, mh.get_node_at(2))
        self.assertEqual(4, mh.get_node_at(3))
        self.assertEqual(2, mh.get_node_at(4))

    def testAppendElementEqualToParent(self):
        mh = MinHeap()

        mh.append(4)
        mh.append(2)
        mh.append(3)
        mh.append(1)

        mh.append(2)
        self.assertEqual(1, mh.get_node_at(0))
        self.assertEqual(2, mh.get_node_at(1))
        self.assertEqual(3, mh.get_node_at(2))
        self.assertEqual(4, mh.get_node_at(3))
        self.assertEqual(2, mh.get_node_at(4))

    def testAppendMultipleUnsortedElements(self):
        mh = MinHeap()

        mh.append(10)
        mh.append(15)
        mh.append(12)
        mh.append(13)
        mh.append(11)
        mh.append(7)

        self.assertEqual(7, mh.get_node_at(0))
        self.assertEqual(11, mh.get_node_at(1))
        self.assertEqual(10, mh.get_node_at(2))
        self.assertEqual(15, mh.get_node_at(3))
        self.assertEqual(13, mh.get_node_at(4))
        self.assertEqual(12, mh.get_node_at(5))

    def testAppendGreaterWithSingleLeftLeafTree(self):
        mh = MinHeap()
        mh.append(0)
        mh.append(1)
        mh.append(2)
        mh.pop_min()
        self.assertEqual(1, mh.get_node_at(0))
        self.assertEqual(2, mh.get_node_at(1))
        self.assertIsNone(mh.get_node_at(2))

        mh.append(10)

        self.assertEqual(1, mh.get_node_at(0))
        self.assertEqual(2, mh.get_node_at(1))
        self.assertEqual(10, mh.get_node_at(2))

    def testAppendLesserWithSingleLeftLeafTree(self):
        mh = MinHeap()
        mh.append(0)
        mh.append(1)
        mh.append(2)
        mh.pop_min()
        self.assertEqual(1, mh.get_node_at(0))
        self.assertEqual(2, mh.get_node_at(1))
        self.assertIsNone(mh.get_node_at(2))

        mh.append(0)

        self.assertEqual(0, mh.get_node_at(0))
        self.assertEqual(2, mh.get_node_at(1))
        self.assertEqual(1, mh.get_node_at(2))


class TestMinHeapPopHead(TestCase):

    def testPopHeadNoElement(self):
        mh = MinHeap()

        self.assertIsNone(mh.pop_min())

    def testPopHeadSingleElement(self):
        mh = MinHeap()
        mh.append(0)

        self.assertEqual(0, mh.pop_min())
        self.assertIsNone(mh.pop_min())

    def testPopHeadTwoReorderedElements(self):
        mh = MinHeap()
        mh.append(1)
        mh.append(0)

        self.assertEqual(0, mh.pop_min())
        self.assertEqual(1, mh.pop_min())
        self.assertIsNone(mh.pop_min())

    def testPopHeadThreeReorderedElements(self):
        mh = MinHeap()
        mh.append(2)
        mh.append(1)
        mh.append(0)

        self.assertEqual(0, mh.pop_min())
        self.assertEqual(1, mh.pop_min())
        self.assertEqual(2, mh.pop_min())
        self.assertIsNone(mh.pop_min())

    def testPopHeadFourReorderedElements(self):
        mh = MinHeap()
        mh.append(4)
        mh.append(2)
        mh.append(3)
        mh.append(1)

        self.assertEqual(1, mh.get_node_at(0))
        self.assertEqual(1, mh.pop_min())
        self.assertEqual(2, mh.pop_min())
        self.assertEqual(3, mh.pop_min())
        self.assertEqual(4, mh.pop_min())

    def testPopHeadMultipleReorderedElements(self):
        mh = MinHeap()
        mh.append(10)
        mh.append(15)
        mh.append(12)
        mh.append(13)
        mh.append(11)
        mh.append(7)
        self.assertEqual(7, mh.get_node_at(0))
        self.assertEqual(11, mh.get_node_at(1))
        self.assertEqual(10, mh.get_node_at(2))
        self.assertEqual(15, mh.get_node_at(3))
        self.assertEqual(13, mh.get_node_at(4))
        self.assertEqual(12, mh.get_node_at(5))

        self.assertEqual(7, mh.pop_min())
        self.assertEqual(10, mh.get_node_at(0))
        self.assertEqual(11, mh.get_node_at(1))
        self.assertEqual(12, mh.get_node_at(2))
        self.assertEqual(15, mh.get_node_at(3))
        self.assertEqual(13, mh.get_node_at(4))
        self.assertIsNone(mh.get_node_at(5))

        self.assertEqual(10, mh.pop_min())
        self.assertEqual(11, mh.get_node_at(0))
        self.assertEqual(13, mh.get_node_at(1))
        self.assertEqual(12, mh.get_node_at(2))
        self.assertEqual(15, mh.get_node_at(3))
        self.assertIsNone(mh.get_node_at(4))

        self.assertEqual(11, mh.pop_min())
        self.assertEqual(12, mh.get_node_at(0))
        self.assertEqual(13, mh.get_node_at(1))
        self.assertEqual(15, mh.get_node_at(2))
        self.assertIsNone(mh.get_node_at(3))

        self.assertEqual(12, mh.pop_min())
        self.assertEqual(13, mh.get_node_at(0))
        self.assertEqual(15, mh.get_node_at(1))
        self.assertIsNone(mh.get_node_at(2))

        self.assertEqual(13, mh.pop_min())
        self.assertEqual(15, mh.get_node_at(0))
        self.assertIsNone(mh.get_node_at(1))

    def testSingleLeftLeafAfterPopHead(self):
        mh = MinHeap()
        mh.append(0)
        mh.append(1)
        mh.append(2)

        self.assertEqual(0, mh.pop_min())
        self.assertEqual(1, mh.get_node_at(0))
        self.assertEqual(2, mh.get_node_at(1))
        self.assertIsNone(mh.get_node_at(2))
