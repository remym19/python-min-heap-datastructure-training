# todo: extract to its own module for reuse

def _is_root_index(index):
    return index == 0


def _get_parent_index(child_index):
    return (child_index - 1) // 2


def _get_left_child_index(parent_index):
    return 2 * parent_index + 1


def _get_right_child_index(parent_index):
    return 2 * parent_index + 2



class MinHeap:

    def __init__(self) -> None:
        super().__init__()
        self._binary_tree = []

    def __len__(self):
        return len(self._binary_tree)

    def is_empty(self):
        return len(self._binary_tree) == 0

    def get_node_at(self, i):
        if i >= len(self._binary_tree):
            return None
        return self._binary_tree[i]

    def append(self, element):
        self._binary_tree.append(element)
        self._heapify_towards_root(self._get_last_index())

    def _heapify_towards_root(self, index):
        if _is_root_index(index):
            return

        parent_index = _get_parent_index(index)
        if self._is_less_than_by_indexes(index, parent_index):
            self._switch_by_index(index, parent_index)
            self._heapify_towards_root(parent_index)

    def peek_min(self):
        return self.get_node_at(0)

    def pop_min(self):
        if self.is_empty():
            return None
        elif len(self._binary_tree) == 1:
            return self._binary_tree.pop(0)
        else:
            popped = self._binary_tree[0]
            self._binary_tree[0] = self._binary_tree.pop(self._get_last_index())
            self._heapify_towards_leaves_from(0)
            return popped

    def _heapify_towards_leaves_from(self, i):
        left_child_index = _get_left_child_index(i)
        right_child_index = _get_right_child_index(i)

        destination = i
        if self._is_index_within_tree(left_child_index) and \
                self._is_index_out_of_tree(right_child_index):
            destination = self._get_index_of_lowest_element(i, left_child_index)
        elif self._is_index_within_tree(right_child_index):
            candidate = self._get_index_of_lowest_element(left_child_index, right_child_index)
            destination = self._get_index_of_lowest_element(i, candidate)

        if destination != i:
            self._switch_by_index(i, destination)
            self._heapify_towards_leaves_from(destination)

    def _get_index_of_lowest_element(self, i1, i2):
        if self._is_less_than_by_indexes(i1, i2):
            return i1
        else:
            return i2

    def _is_index_within_tree(self, index):
        return index <= self._get_last_index()

    def _is_index_out_of_tree(self, index):
        return not self._is_index_within_tree(index)

    def _get_last_index(self):
        return len(self._binary_tree) - 1

    def _is_less_than_by_indexes(self, i1, i2):
        return self._binary_tree[i1] < self._binary_tree[i2]

    def _switch_by_index(self, i1, i2):
        self._binary_tree[i1], self._binary_tree[i2] = self._binary_tree[i2], self._binary_tree[i1]
